# Rest-Countries React project.

## About project.
* In this project, you can search for info about any country in the search bar.
* Sort the data according to region, subregion, name, and ascending and descending order.
* Getting data from API fetching, When the user clicks on any country, that particular country info will be showing.

## Hosted Link
https://main--boisterous-druid-ec9977.netlify.app/

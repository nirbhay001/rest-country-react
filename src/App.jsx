import { BrowserRouter ,Routes, Route} from "react-router-dom";
import "./App.css";
import Country from "./components/Country";
import Home from "./components/HomeComponent/Home";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<Country/>}></Route>
        <Route path="/home/:countryName" element={<Home />}></Route>
      </Routes>   
      </BrowserRouter>
    </div>
  );
}

export default App;

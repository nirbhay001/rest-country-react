import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';

function CountryContainer({countryList}) {
  return (
    <div >
      {countryList && countryList.length===0 ? <h1 className="d-flex justify-content-center">Country not Found</h1>: <ul className="d-flex flex-wrap justify-content between list-unstyled ">{countryList}</ul>} 
    </div>
  )
}

export default CountryContainer

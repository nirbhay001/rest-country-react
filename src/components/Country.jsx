import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import ListCountry from "./ListCountry";
import {InfinitySpin } from "react-loader-spinner";
import 'bootstrap/dist/css/bootstrap.css';

function Country() {
  const [data, setData] = useState([]);
  const [subRegion, setSubRegion] = useState([]);
  const [resetData, setResetData] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [optionRegionValue, setOptionValue] = useState("");
  const [optionSubRegionValue, setOptionSubRegionValue] = useState("");
  const [sortText, setSortText] = useState("");
  const sortArray =["area","population","name"];

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setResetData(data);
      })
      .catch((error) => console.log(error));
  }, []);

  const filterRegion=resetData.reduce((accumulator, current) => {
    if (!accumulator.includes(current.region) && current.region!=='Antarctic'
    ) {
      accumulator.push(current.region);
    }
    return accumulator;
  }, []);

  function handleSelect(event) {
    let regionData = event.target.value;
    if (regionData.length === 0) {
      setData(resetData);
      setOptionValue(regionData);
      setSubRegion([]);
      setSearchText("");
      return;
    }
    let filterRegionData = resetData.filter((item) => {
      return item.region === regionData;
    });
    setData(filterRegionData);
    const allSubRegion = resetData.reduce((accumulator, current) => {
      if (
        current.region === regionData &&
        !accumulator.includes(current.subregion)
      ) {
        accumulator.push(current.subregion);
      }
      return accumulator;
    }, []);
    setSubRegion(allSubRegion);
    setOptionValue(regionData);
  }

  function searchInput(e) {
    let text = e.target.value;
    setSearchText(e.target.value);
    if (optionRegionValue.length === 0) {
      let filterSearchData = resetData.filter((item) => {
        return (
          item.name.official.toLowerCase().indexOf(text.toLowerCase()) !== -1
        );
      });
      setData(filterSearchData);
    } else {
      let filterSearchData = resetData.filter((item) => {
        if (optionSubRegionValue.length === 0) {
          return (
            item.name.official.toLowerCase().indexOf(text.toLowerCase()) !==
              -1 && item.region === optionRegionValue
          );
        } else if (optionSubRegionValue.length !== 0) {
          return (
            item.name.official.toLowerCase().indexOf(text.toLowerCase()) !==
              -1 &&
            item.region === optionRegionValue &&
            item.subregion === optionSubRegionValue
          );
        }
      });
      setData(filterSearchData);
    }
  }
  function handleSelectSubRegion(e) {
    let subRegionData = e.target.value;
    if (subRegionData.length === 0 && optionRegionValue.length === 0) {
      setData(resetData);
      return;
    } else if (subRegionData.length === 0 && optionRegionValue.length !== 0) {
      let filterRegionData = resetData.filter((item) => {
        return item.region === optionRegionValue;
      });
      setData(filterRegionData);
    } else {
      let filterSubRegion = resetData.filter((item) => {
        return (
          item.subregion === subRegionData && item.region === optionRegionValue
        );
      });
      setData(filterSubRegion);
    }

    setOptionSubRegionValue(subRegionData);
  }
  function sortOrder(e) {
    let sortSign;
    let valueData;
    let sortValue = e.target.value;
    sortValue === "inc" ? (sortSign = true) : (sortSign = false);
    let newData = [...data];
    if (sortText.length === 0) {
      return;
    } else if (sortText === "name") {
      let sortItem = newData.sort((a, b) =>a.name.official < b.name.official? sortSign?  1: -1: sortSign ? -1 : 1);
      setData(sortItem);
    } else {
      let sortItem = newData.sort((a, b) =>
        sortSign? a[sortText] - b[sortText]: valueData = b[sortText] - a[sortText]);
      setData(sortItem);
    }
  }

  function sortCountryData(e) {
    let sortValue = e.target.value;
    setSortText(sortValue);
    if (sortValue.length === 0) {
      setSortText("");
      return;
    }

    let newData = [...data];
    if (sortValue === "name") {
      let sortItem = newData.sort((a, b) =>
        a.name.official > b.name.official ? 1 : -1
      );
      setData(sortItem);
    } else if (sortValue === "population" || sortValue === "area") {
      let sortItem = newData.sort((a, b) => {
        return b[sortValue] - a[sortValue];
      });
      setData(sortItem);
    }
  }
  return (
    <>
      {resetData && resetData.length == 0 ? (
        <div className="d-flex justify-content-center">
        <InfinitySpin width="200"  color="#4fa94d" />
        </div>
      ) : (
        <ListCountry
          data={data}
          searchText={searchText}
          searchInput={searchInput}
          handleSelect={handleSelect}
          handleSelectSubRegion={handleSelectSubRegion}
          sortCountryData={sortCountryData}
          subRegion={subRegion}
          sortOrder={sortOrder}
          filterRegion={filterRegion}
          sortArray={sortArray}
        />
      )}
    </>
  );
}
export default Country;

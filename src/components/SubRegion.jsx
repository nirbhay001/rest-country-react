import React from "react";

function SubRegion({ subRegion, handleSelectSubRegion }) {
  return (
    <>
      <select onClick={handleSelectSubRegion} className="optionList">
        <option key={"subregion"} value="">Select subregion</option>
        {subRegion.map((item) => {
          return <option key={item} value={item}>{item}</option>;
        })}
      </select>
    </>
  );
}

export default SubRegion;

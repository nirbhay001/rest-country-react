import React from 'react'

function Region({handleSelect,filterRegion}) {
  return (
    <select onClick={handleSelect} className="optionList">
        <option key="region" value="">Select Region</option>
        {filterRegion.map((item) => {
          return <option key={item} value={item}>{item}</option>;
        })}
  </select>
  )
}
export default Region

import React from 'react'
import 'bootstrap/dist/css/bootstrap.css';
import InputSearch from '../InputSearch';
import Region from '../Region';
import SubRegion from '../SubRegion';
import SortCountry from '../SortCountry';
import OrderSort from '../OrderSort';
import './SearchBar.css'

function SearchBar({subRegion, searchText,searchInput,handleSelect,handleSelectSubRegion,sortCountryData,sortOrder, filterRegion, sortArray}) {
  return (
    <div className="searchBar">
      <InputSearch searchText={searchText} searchInput={searchInput}/>
      <div className='searchAccording'>
        <Region  handleSelect={handleSelect} filterRegion={filterRegion }/>
        <SubRegion subRegion={subRegion} handleSelectSubRegion={handleSelectSubRegion} />
        <SortCountry sortCountryData={sortCountryData} sortArray={sortArray} />
        <OrderSort sortOrder={sortOrder}/>
      </div>
    </div>
  )
}

export default SearchBar


import React from "react";
import Card from "./Card";
import SearchBar from "./SearchBar/SearchBar";
import CountryContainer from "./CountryContainer";
import Header from "./HeaderComponent/Header";
import 'bootstrap/dist/css/bootstrap.css';

function ListCountry({ data,searchText,searchInput,handleSelect,handleSelectSubRegion,sortCountryData, subRegion, sortOrder, sortText,filterRegion,sortArray}) {
 
  let countryList = data.map((countryItem, index) => {
   return <Card countryItem={countryItem} key={index}/>
  });
  return (
    <>
    <Header/>
    <SearchBar subRegion={subRegion} searchText={searchText} searchInput={searchInput} handleSelect={handleSelect} handleSelectSubRegion={handleSelectSubRegion} sortCountryData={sortCountryData} sortOrder={sortOrder} filterRegion={filterRegion} sortArray={sortArray} />
    <CountryContainer countryList={countryList}/>
    </>
  );
}
export default ListCountry;







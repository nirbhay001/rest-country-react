import React from 'react'

function OrderSort({sortOrder}) {
  return (
    <select onClick={sortOrder} className="optionList">
      <option key="sortOrder" value="">Sort by Order</option>
      <option key="inc" value="inc">Increasing</option>
      <option key="dec" value="dec">Decreasing</option>
    </select>
  )
}

export default OrderSort

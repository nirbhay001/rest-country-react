import React from "react";
import 'bootstrap/dist/css/bootstrap.css';

function InputSearch({ searchText, searchInput }) {
  return (
      <input
        type="text"
        value={searchText}
        onChange={searchInput}
        className="form-control-lg rounded"
        placeholder="Search by Country Name"
        aria-label="Search"
        aria-describedby="basic-addon2"
      />
  );
}

export default InputSearch;

import React from "react";

function SortCountry({sortCountryData, sortArray }) {

  return (
    <select onClick={sortCountryData} className="optionList">
      <option key="sortCountry" value="">Sort Country</option>
      {sortArray.map((item) => {
          return <option key={item} value={item}>{item}</option>;
        })}

    </select>
  );
}

export default SortCountry;

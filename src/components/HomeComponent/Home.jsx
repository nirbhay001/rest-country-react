import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import {InfinitySpin } from "react-loader-spinner";
import Header from "../HeaderComponent/Header";
import './Home.css'

function Home() {
  let { countryName } = useParams();

  const [currentCountry, setCurrentCountry] = useState([]);

  useEffect(() => {
      fetch(`https://restcountries.com/v3.1/name/${countryName}`)
      .then((response)=>response.json())
      .then((data)=>setCurrentCountry(data))
  }, [countryName]);

  return currentCountry.length === 0 ? (
    <div className="d-flex justify-content-center">
    <InfinitySpin width="200" color="#4fa94d" />
    </div>
  ) : (
    <div className="backgroundStyle">
      <Header/>
      <Link to="/">
        <button type="button" className="button backButton buttonMargin">
        <span>&#8592;</span>Back
        </button>
      </Link>
      <div className="imageContainer">
        <img src={currentCountry[0].flags.png} alt="country"  className="countryImage"/>
        <div >
          <h1 >{currentCountry[0].name.common}</h1>
          <div className="countryDataStyle">
            <div>
              <p >
                Native Name{"  "}:{"  "}
                <span >{Object.entries(Object.entries(Object.entries(currentCountry[0].name)[2])[1][1])[0][1].official}</span>
              </p>
              <p >
                Population{"  "}:{"  "}
                <span >{currentCountry[0].population}</span>
              </p>
              <p >
                Region{"  "}: {"  "}<span >{currentCountry[0].region}</span>
              </p>
              <p >
                subRegion{"  "}:{"  "}
                <span >{currentCountry[0].subregion}</span>
              </p>
              <p >
                Capital{"  "}:{"  "}
                <span >{currentCountry[0].capital[0]}</span>
              </p>
            </div>
            <div>
              <p >
                Top Level Domains{"  "}:{"  "}
                <span >{currentCountry[0].tld[0]}</span>
              </p>
              <p >
                Currencies{"  "}:{"  "}
                <span >
                  {currentCountry[0].currencies !== undefined
                    ? Object.entries(currentCountry[0].currencies)[0][1].name
                    : "no currencies found"}
                </span>
              </p>
              <p >
                Languages{"  "}:{"  "}
                <span >
                  {Object.values(currentCountry[0].languages)
                    .reverse()
                    .join(",")}
                </span>
              </p>
            </div>
          </div>
          <label>Border Countries :</label>
          <button className="buttonMargin button">
            {currentCountry[0].borders !== undefined
              ? currentCountry[0].borders[0]
              : "No border found"}
          </button>
          <button className="buttonMargin button">
            {currentCountry[0].borders !== undefined
              ? currentCountry[0].borders[1]
              : "No border found"}
          </button>
          <button className="buttonMargin button">
            {currentCountry[0].borders !== undefined
              ? currentCountry[0].borders[2]
              : "No border found"}
          </button>
        </div>
      </div>
    </div>
  );
}

export default Home;
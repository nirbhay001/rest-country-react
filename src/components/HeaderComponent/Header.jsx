import React from "react";
import "./Header.css";
import imageIs from "../../assets/moon.jpg";

function Header() {
  return (
    <>
      <nav className="header">
        <h1>Where is the World</h1>
        <img src={imageIs} className="moonImage" alt="moon image" />
      </nav>
    </>
  );
}

export default Header;

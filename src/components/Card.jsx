import React from "react";
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
function Card({ countryItem, index }) {
  return (
    <Link to={`/home/${countryItem.name.common}`}>
      <li key={index}>
        <div className="card m-5" style={{ width: "18rem" }}>
          <img alt="countryImage" src={countryItem.flags.png} />
          <div className="card-body">
          <h3>Name : {countryItem.name.official}</h3>
          <ul>
            <p>Capital : {countryItem.capital}</p>
            <p>Region : {countryItem.region}</p>
            <p>Area : {countryItem.area}</p>
            <p>Population : {countryItem.population}</p>
            </ul>
          </div>
        </div>
      </li>
    </Link>
  );
}

export default Card;
